#pragma checksum "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "f7a5573d374ebf28979f1e030529def3d41f3d2f"
// <auto-generated/>
#pragma warning disable 1591
namespace RazorAndData.Components
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
using RazorAndData.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
using RazorAndData.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
    public partial class ProductList : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "card-columns");
            __builder.OpenElement(2, "div");
            __builder.OpenElement(3, "input");
            __builder.AddAttribute(4, "type", "text");
            __builder.AddAttribute(5, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 12 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                  first

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(6, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => first = __value, first));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(7, "\r\n        ");
            __builder.AddMarkupContent(8, "<label for=\"plus\">+</label>\r\n        ");
            __builder.OpenElement(9, "input");
            __builder.AddAttribute(10, "type", "text");
            __builder.AddAttribute(11, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 14 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                  second

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(12, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => second = __value, second));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(13, "\r\n        ");
            __builder.AddMarkupContent(14, "<label for=\"total\">=</label>\r\n        ");
            __builder.OpenElement(15, "input");
            __builder.AddAttribute(16, "type", "text");
            __builder.AddAttribute(17, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 16 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                  result

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(18, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => result = __value, result));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(19, "\r\n\r\n        ");
            __builder.OpenElement(20, "button");
            __builder.AddAttribute(21, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 18 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                          (e => Sum())

#line default
#line hidden
#nullable disable
            ));
            __builder.AddMarkupContent(22, "\r\n            Sum\r\n        ");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(23, "\r\n    ");
            __builder.OpenElement(24, "button");
            __builder.AddAttribute(25, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 22 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                      (e => Check())

#line default
#line hidden
#nullable disable
            ));
            __builder.AddMarkupContent(26, "\r\n        Check\r\n    ");
            __builder.CloseElement();
#nullable restore
#line 26 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
     foreach (var product in ProductService.GetProducts())
    {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(27, "div");
            __builder.AddAttribute(28, "class", "card");
            __builder.OpenElement(29, "div");
            __builder.AddAttribute(30, "class", "card-body");
            __builder.OpenElement(31, "h5");
            __builder.AddAttribute(32, "class", "card-title");
            __builder.AddContent(33, 
#nullable restore
#line 30 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                        product.Title

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(34, "\r\n                ");
            __builder.OpenElement(35, "h5");
            __builder.AddAttribute(36, "class", "card-title");
            __builder.AddContent(37, 
#nullable restore
#line 31 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                        counter

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(38, "\r\n            ");
            __builder.OpenElement(39, "div");
            __builder.AddAttribute(40, "class", "card-footer");
            __builder.OpenElement(41, "small");
            __builder.AddAttribute(42, "class", "text-muted");
            __builder.OpenElement(43, "button");
            __builder.AddAttribute(44, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 36 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                      (e => SelectProduct(product.Id))

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(45, "data-toggle", "modal");
            __builder.AddAttribute(46, "data-target", "#productModal");
            __builder.AddAttribute(47, "class", "btn btn-primary");
            __builder.AddMarkupContent(48, "\r\n                        More Info\r\n                    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(49, "\r\n                    ");
            __builder.OpenElement(50, "button");
            __builder.AddAttribute(51, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 40 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                      (e => Increase() )

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(52, "class", "btn btn-primary");
            __builder.AddMarkupContent(53, "\r\n                        Increase ");
            __builder.AddContent(54, 
#nullable restore
#line 41 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                  counter

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(55, "\r\n                    ");
            __builder.OpenElement(56, "button");
            __builder.AddAttribute(57, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 43 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                      (e => Decrease() )

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(58, "class", "btn btn-primary");
            __builder.AddMarkupContent(59, "\r\n                        Decrease ");
            __builder.AddContent(60, 
#nullable restore
#line 44 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                  counter

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 49 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
    }

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
#nullable restore
#line 52 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
 if (selectedProduct != null)
{

#line default
#line hidden
#nullable disable
            __builder.OpenElement(61, "div");
            __builder.AddAttribute(62, "class", "modal fade");
            __builder.AddAttribute(63, "id", "productModal");
            __builder.AddAttribute(64, "tabindex", "-1");
            __builder.AddAttribute(65, "role", "dialog");
            __builder.AddAttribute(66, "aria-labelledby", "productTitle");
            __builder.AddAttribute(67, "aria-hidden", "true");
            __builder.OpenElement(68, "div");
            __builder.AddAttribute(69, "class", "modal-dialog modal-dialog-centered");
            __builder.AddAttribute(70, "role", "document");
            __builder.OpenElement(71, "div");
            __builder.AddAttribute(72, "class", "modal-content");
            __builder.OpenElement(73, "div");
            __builder.AddAttribute(74, "class", "modal-header");
            __builder.OpenElement(75, "h5");
            __builder.AddAttribute(76, "class", "modal-title");
            __builder.AddAttribute(77, "id", "productTitle");
            __builder.AddContent(78, 
#nullable restore
#line 58 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                                               selectedProduct.Title

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(79, "&nbsp;\r\n                    ");
            __builder.OpenElement(80, "input");
            __builder.AddAttribute(81, "type", "text");
            __builder.AddAttribute(82, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 59 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                              newTitle

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(83, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => newTitle = __value, newTitle));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(84, "&nbsp;\r\n                    ");
            __builder.OpenElement(85, "button");
            __builder.AddAttribute(86, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 60 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                      (e =>changeTitle (newTitle))

#line default
#line hidden
#nullable disable
            ));
            __builder.AddMarkupContent(87, "\r\n                        Change Title!\r\n                    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(88, "\r\n                    ");
            __builder.AddMarkupContent(89, "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>");
            __builder.CloseElement();
            __builder.AddMarkupContent(90, "\r\n                ");
            __builder.OpenElement(91, "div");
            __builder.AddAttribute(92, "class", "modal-body");
            __builder.OpenElement(93, "div");
            __builder.AddAttribute(94, "class", "card");
            __builder.OpenElement(95, "div");
            __builder.AddAttribute(96, "class", "card-body");
            __builder.OpenElement(97, "p");
            __builder.AddAttribute(98, "class", "card-text");
            __builder.AddContent(99, 
#nullable restore
#line 71 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                                  selectedProduct.Body

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(100, "\r\n                            ");
            __builder.OpenElement(101, "p");
            __builder.AddAttribute(102, "class", "card-text");
            __builder.AddContent(103, "Id: ");
            __builder.AddContent(104, 
#nullable restore
#line 72 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                                      selectedProduct.Id

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(105, "\r\n                            ");
            __builder.OpenElement(106, "p");
            __builder.AddAttribute(107, "class", "card-text");
            __builder.AddContent(108, "UserId: ");
            __builder.AddContent(109, 
#nullable restore
#line 73 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                                          selectedProduct.UserId

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(110, "\r\n                ");
            __builder.OpenElement(111, "div");
            __builder.AddAttribute(112, "class", "modal-footer");
            __builder.OpenElement(113, "button");
            __builder.AddAttribute(114, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 78 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                      (e =>deleteRating ())

#line default
#line hidden
#nullable disable
            ));
            __builder.AddMarkupContent(115, "\r\n                        Delete Rating\r\n                    ");
            __builder.CloseElement();
#nullable restore
#line 82 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                     if (voteCount == 0)
                    {

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(116, "<span>Be the first to vote!</span>");
#nullable restore
#line 85 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                    }
                    else
                    {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(117, "span");
            __builder.AddContent(118, 
#nullable restore
#line 88 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                               voteCount

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(119, " ");
            __builder.AddContent(120, 
#nullable restore
#line 88 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                          voteLabel

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
#nullable restore
#line 89 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                    }

#line default
#line hidden
#nullable disable
#nullable restore
#line 90 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                     for (int i = 1; i < 6; i++)
                    {
                        var currentStar = i;
                        if (i <= currentRating)
                        {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(121, "span");
            __builder.AddAttribute(122, "class", "fa fa-star checked");
            __builder.AddAttribute(123, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 95 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                                                       (e => SubmitRating(currentStar))

#line default
#line hidden
#nullable disable
            ));
            __builder.CloseElement();
#nullable restore
#line 96 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                        }
                        else
                        {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(124, "span");
            __builder.AddAttribute(125, "class", "fa fa-star");
            __builder.AddAttribute(126, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 99 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                                                               (e => SubmitRating(currentStar))

#line default
#line hidden
#nullable disable
            ));
            __builder.CloseElement();
#nullable restore
#line 100 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
                        }
                    }

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 106 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
}

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(127, "<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">");
        }
        #pragma warning restore 1998
#nullable restore
#line 111 "D:\Users\aussarapornpat\Downloads\SampleWebsite3\RazorAndData\Components\ProductList.razor"
 
    String newTitle = "";

    void changeTitle(string newTitle)
    {
        selectedProduct.Title = newTitle;
        ProductService.ChangeTitle(selectedProductId, newTitle);
        SelectProduct(selectedProductId);
    }




    int first = 1;
    int second = 0;
    int result = 0;

    void Sum()
    {
        result = first + second;
        System.Diagnostics.Debug.WriteLine(result);
    }

    int counter = 0;
    void Increase()
    {
        counter++;
        first++;
    }

    void Decrease()
    {
        counter--;
        first--;
    }

    void Check()
    {
        System.Diagnostics.Debug.WriteLine(first);
    }

    Product selectedProduct;
    int selectedProductId;

    void SelectProduct(int productId)
    {
        selectedProductId = productId;
        selectedProduct = ProductService.GetProducts().First(x => x.Id == productId);
        GetCurrentRating();
    }

    int currentRating = 0;
    int voteCount = 0;
    string voteLabel;

    void GetCurrentRating()
    {
        if (selectedProduct.Ratings == null)
        {
            currentRating = 0;
            voteCount = 0;
        }
        else
        {
            voteCount = selectedProduct.Ratings.Count();
            voteLabel = voteCount > 1 ? "Votes" : "Vote";

            if (selectedProduct.Ratings.Count() > 0)
            {
                currentRating = selectedProduct.Ratings.Sum() / voteCount;
            }
            else
            {
                currentRating = 0;
            }

        }

        System.Console.WriteLine($"Current rating for {selectedProduct.Id}: {currentRating}");
    }

    void SubmitRating(int rating)
    {
        System.Console.WriteLine($"Rating received for {selectedProduct.Id}: {rating}");
        ProductService.AddRating(selectedProductId, rating);
        SelectProduct(selectedProductId);
    }


    void deleteRating()
    {
        voteCount = 0;
        ProductService.ClearRating(selectedProductId);
    }


#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private JsonFileProductService ProductService { get; set; }
    }
}
#pragma warning restore 1591
