﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using RazorAndData.Models;
using Microsoft.AspNetCore.Hosting;

namespace RazorAndData.Services
{
    public class JsonFileProductService
    {
        public JsonFileProductService(IWebHostEnvironment webHostEnvironment)
        {
            WebHostEnvironment = webHostEnvironment;
        }

        public IWebHostEnvironment WebHostEnvironment { get; }

        private string JsonFileName
        {
            get { return Path.Combine(WebHostEnvironment.WebRootPath, "data", "products.json"); }// C:\Users\iHAVECPU\source\repos\SampleWebsite\RazorAndData\wwwroot\data\products.json
        }

        public IEnumerable<Product> GetProducts()
        {
            using (var jsonFileReader = File.OpenText(JsonFileName))
            {
                return JsonSerializer.Deserialize<Product[]>(jsonFileReader.ReadToEnd(),
                    new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true
                    });
            }
        }

        public void AddRating(int productId, int rating)
        {
            var products = GetProducts();
            var query = products.First(x => x.Id == productId);
            if (query.Ratings == null)
            {
                query.Ratings = new int[] { rating };
            }
            else
            {
                var ratings = query.Ratings.ToList();
                ratings.Add(rating);
                query.Ratings = ratings.ToArray();
            }
            using (var outputStream = File.OpenWrite(JsonFileName))
            {
                JsonSerializer.Serialize<IEnumerable<Product>>(
                        new Utf8JsonWriter(outputStream, new JsonWriterOptions
                        {
                            SkipValidation = true,
                            Indented = true
                        }),
                        products
                    );
            }
        }


        public void ClearRating(int productId)
        {
            var products = GetProducts();

            var query = products.First(x => x.Id == productId);
            if (query.Ratings != null)
            {
                query.Ratings = new int[] { };

                //var ratings = query.Ratings.ToList();
                //ratings.Clear();
                //query.Ratings = ratings.ToArray();
            }
            if (File.Exists(JsonFileName) == true)
            {
                File.Delete(JsonFileName);
            }

            using (var outputStream = File.Create(JsonFileName))
            {
                JsonSerializer.Serialize<IEnumerable<Product>>(
                        new Utf8JsonWriter(outputStream, new JsonWriterOptions
                        {
                            SkipValidation = true,
                            Indented = true,
                        }),
                        products
                    );
            }
        }

        public void ChangeTitle(int productId , string productTitle)
        {
            var products = GetProducts();

            var query = products.First(x => x.Id == productId);

            if (query.Title != null)
            {
                query.Title = productTitle;
            }

            if (File.Exists(JsonFileName) == true)
            {
                File.Delete(JsonFileName);
            }
           
            using (var outputStream = File.Create(JsonFileName))
            {
                JsonSerializer.Serialize<IEnumerable<Product>>(
                        new Utf8JsonWriter(outputStream, new JsonWriterOptions
                        {
                            SkipValidation = true,
                            Indented = true
                        }),
                        products
                    );
            }

        }

    }
}
