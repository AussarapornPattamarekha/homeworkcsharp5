﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace SampleWebsite.Models
{
    public class Product
    {
        public int UserId { get; set; }
        public int Id { get; set; }

        public string Title { get; set; }

        public string Body { get; set; }

        public override string ToString() => JsonSerializer.Serialize<Product>(this);
    }
}
